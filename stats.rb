#!/usr/bin/env ruby

require 'qiita'

class QiitaAgent
  @client = nil
  def initialize
    args = {access_token: ENV['QIITA_ACCESS_TOKEN'], team: ENV['QIITA_TEAM_NAME']}
    @client = Qiita::Client.new(args)
  end

  def get_all_items
    last_response = get_items
    return nil if last_response.status != 200 

    total_items = last_response.headers["Total-Count"].to_i
    page_labels = get_page_labels(last_response.headers["Link"])

    data = last_response.body
    return data if page_labels["last"] < 2

    (2..page_labels["last"]).each do |p|
      data.concat(get_items(p).body)
    end

    total_items == data.size ? data : nil
  end

  def get_users(page = nil)
    page ? @client.list_users(page: page) : @client.list_users
  end

  private
  def get_items(page = nil)
    page ? @client.list_items(page: page) : @client.list_items
  end

  def get_page_labels(link_str)
    page_labels = {}
    link_str.split(',').each do |l|
      md = l.match(/page=(\d+)>; rel="([^"]+)"/)
      page_labels[md[2]] = md[1].to_i if md[1] && md[2]
    end
    return page_labels
  end
end

qiita = QiitaAgent.new
user = Struct.new("User",:id)
users = []
#チームメンバーの一覧取得
qiita.get_users.body.each do |u|
  users << user.new(u["id"])
end
p users

#投稿情報を取得
data = qiita.get_all_items
exit if data.nil?

record = Struct.new("Record",:id,:created_date)
table = []
data.each do |d|
  table << record.new(d["user"]["id"], Time.parse(d["created_at"]).strftime("%Y-%m-%d"))
end

#日別の投稿件数
p table.inject(Hash.new(0)) {|h,r| h[r.created_date] += 1;h}

#日別の人別投稿件数
p table.inject(Hash.new { |hash,key| hash[key] = Hash.new {0} }) {|h,r| h[r.created_date][r.id] += 1;h}
